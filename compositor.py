import numpy as np
import cv2
import json
import os
import random
import time


class Compositor:
    """This class can compose renderings of objects with random background images."""

    def __init__(self, config):
        # output path (and check if exists)
        self.composites_path = config['General']['composites_directory']
        self.renderings_path = config['General']['renderings_directory']

        # read other configs for compositing images
        cfg = config['Compositor']
        self.bg_images_dir = cfg['bg_images_dir']
        self.objects_per_image = int(cfg['objects_per_image'])
        self.images_per_object = int(cfg['images_per_object'])
        self.image_size_x = int(cfg['image_size_x'])
        self.image_size_y = int(cfg['image_size_y'])
        self.smooth_obj_boundaries = cfg['smooth_obj_boundaries'].lower() in ['true', '1', 'yes', 'y']

        self.time_for_overlay = 0
        self.time_for_imwrite = 0

    @staticmethod
    def prepare_bg_image(image, width, height):
        """This method will prepare the image so that it has the desired size.

        Particularly, it will not distort anything - if (w*h) aspect ratio is different, it will make a random crop.
        It will also try to rotate the picture if that fits better."""

        # check rotation
        (h, w) = image.shape[:2]
        aspect_ratio = w/h
        desired_aspect_ratio = width/height
        if (aspect_ratio < 1 < desired_aspect_ratio) or (aspect_ratio > 1 > desired_aspect_ratio):
            image = np.rot90(image)
            (h, w) = image.shape[:2]

        # now check that image size is large enough
        scale_factor = max(height / h, width / w)
        if scale_factor > 1:
            # we need to scale up the image
            image = cv2.resize(image, None, fx=scale_factor, fy=scale_factor)

        # now all we need to do is to crop
        image = image[0:height, 0:width]
        return image

    @staticmethod
    def overlay_image_alpha(img, img_overlay, pos, alpha_mask):
        """Overlay img_overlay on top of img at the position specified by
        pos and blend using alpha_mask.

        Alpha mask must contain values within the range [0, 1] and be the
        same size as img_overlay.
        Pos is x, y and specifies position of the top left of img_overlay in img.

        Note that I adapted this function from here:
        https://stackoverflow.com/questions/14063070/overlay-a-smaller-image-on-a-larger-image-python-opencv
        """

        x, y = pos

        # Image ranges
        y1, y2 = max(0, y), min(img.shape[0], y + img_overlay.shape[0])
        x1, x2 = max(0, x), min(img.shape[1], x + img_overlay.shape[1])

        # Overlay ranges
        y1o, y2o = max(0, -y), min(img_overlay.shape[0], img.shape[0] - y)
        x1o, x2o = max(0, -x), min(img_overlay.shape[1], img.shape[1] - x)

        # Exit if nothing to do
        if y1 >= y2 or x1 >= x2 or y1o >= y2o or x1o >= x2o:
            return

        channels = img.shape[2]

        alpha = alpha_mask[y1o:y2o, x1o:x2o]
        alpha_inv = 1.0 - alpha

        for c in range(channels):
            img[y1:y2, x1:x2, c] = (alpha * img_overlay[y1o:y2o, x1o:x2o, c] +
                                    alpha_inv * img[y1:y2, x1:x2, c])

    def compose(self):
        print('yes i am composing something.. shalalala')

        # read ground truth from rendering
        render_gt_file = os.path.join(self.renderings_path, 'ground_truth.txt')
        with open(render_gt_file, 'r') as infile:
            render_gt_dict = json.load(infile)

        # get number of objects
        objects = render_gt_dict['image_labels']
        num_images = len(objects) * self.images_per_object // self.objects_per_image

        print(f'- found {len(objects):d} objects')
        print(f'- pasting each object {self.images_per_object:d} times')
        print(f'- pasting {self.objects_per_image:d} objects in each image')
        print(f'- will create {num_images:d} images from that')

        # create list of indices we will use to generate composita
        object_indices = []
        for idx, obj in enumerate(objects):
            object_indices.extend([idx for _ in range(self.images_per_object)])
        # let's shuffle the list
        random.shuffle(object_indices)

        # create list of background images
        bg_images = os.listdir(self.bg_images_dir)
        random.shuffle(bg_images)
        print(f'- found {len(bg_images):d} background images from which i will choose randomly')

        # actually create the images
        for img_idx in range(num_images):
            # read and prepare bg image
            img_fn = os.path.join(self.bg_images_dir, bg_images[img_idx])
            img = cv2.imread(img_fn)
            img = Compositor.prepare_bg_image(img, self.image_size_x, self.image_size_y)

            # place objects
            for _ in range(self.objects_per_image):
                # the last image could have less instances in it, avoid an exception here
                if not object_indices:
                    continue

                # now get the object and read the images
                obj = objects[object_indices.pop()]
                color_fn = os.path.join(self.renderings_path, obj['color_image'])
                mask_fn = os.path.join(self.renderings_path, obj['object_mask'])
                obj_img = cv2.imread(color_fn)
                obj_mask = cv2.imread(mask_fn, cv2.IMREAD_GRAYSCALE)

                if self.smooth_obj_boundaries:
                    # we smooth the mask with gaussian blur
                    # this will effectively reduce the mask value at boundaries, which is used as alpha during overlay
                    # BORDER_CONSTANT should be 0 by default
                    obj_mask = cv2.GaussianBlur(obj_mask, (5, 5), 0, borderType=cv2.BORDER_CONSTANT)

                # let's minimize the object image to only use its bounding box
                pixelpoints = cv2.findNonZero(obj_mask)
                x, y, w, h = cv2.boundingRect(pixelpoints)
                obj_img = obj_img[y:y + h, x:x + w]
                obj_mask = obj_mask[y:y + h, x:x + w]

                # choose random position and do the overlay
                x = random.randrange(0, self.image_size_x - w)
                y = random.randrange(0, self.image_size_y - h)
                start_time = time.clock()
                Compositor.overlay_image_alpha(img, obj_img, (x, y), obj_mask / 255)
                self.time_for_overlay += (time.clock() - start_time)

            img_fn = os.path.join(self.composites_path, f'{img_idx:05d}.png')
            start_time = time.clock()
            cv2.imwrite(img_fn, img)
            self.time_for_imwrite += (time.clock() - start_time)
            print(f'-- saved {img_fn}')

        # todo
        #   create ground truth file
        #   compute ground truth position, also 2d bounding box could be useful
        #   optionally: change scale of object and adjust ground truth ..


def main():
    # here might be some unit tests or whatever
    pass


if __name__ == '__main__':
    main()
