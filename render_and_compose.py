import sys
import os
import configparser
import shutil
import time
from renderer import ModelViewRenderer
from compositor import Compositor

CONFIG_FILE = 'example.cfg'


def clear_directory(directory):
    """Clears all files and subdirectories from a directory."""
    for the_file in os.listdir(directory):
        file_path = os.path.join(directory, the_file)
        if os.path.isfile(file_path):
            os.unlink(file_path)
        elif os.path.isdir(file_path):
            shutil.rmtree(file_path)


def directory_ready_to_write(directory):
    """Makes sure we can write into the directory.

    To be more precise:
    Checks if directory exists, if not, then it will be created.
    If yes, we check if it's empty, if not, user will be prompted.

    Returns:
        True: if we can write to the directory.
        False: if we should not write to the directory.
    """
    print('Preparing directory:', directory)

    try:
        if os.path.exists(directory):
            if os.listdir(directory):
                print('WARNING: directory not empty, possibly overwriting files in ', directory)
                print('Do you want to remove all existing files? (y/n)')
                if 'y' == input():
                    clear_directory(directory)
                    print('Directory cleared.')
                else:
                    print('Did not remove anything. Abort.')
                    return False
        else:
            os.makedirs(directory)

        return True
    except Exception as e:
        print(e)
        return False


def main(config_fn):
    # read config stuff
    print('using config in:', config_fn)
    cfg = configparser.ConfigParser()
    cfg.read(config_fn)

    # check program flow
    render = cfg['General']['render']
    compose = cfg['General']['compose']

    if render.lower() in ['true', '1', 'yes', 'y']:
        print('Renderer ON.')
        if not directory_ready_to_write(cfg['General']['renderings_directory']):
            return

        start_time = time.clock()
        renderer = ModelViewRenderer(cfg)
        renderer.visualize_camera_positions()
        renderer.generate_renderings()
        end_time = time.clock()
        print(f'rendering took {(end_time - start_time) * 1000:.2f} ms')

    else:
        print('Renderer OFF.')

    if compose.lower() in ['true', '1', 'yes', 'y']:
        print('Compositor ON.')
        if not directory_ready_to_write(cfg['General']['composites_directory']):
            return

        start_time = time.clock()
        compositor = Compositor(cfg)
        compositor.compose()
        end_time = time.clock()
        print(f'compositing took {(end_time - start_time) * 1000:.2f} ms')
        print(f'overlaying took {compositor.time_for_overlay * 1000:.2f} ms')
        print(f'imwrite took {compositor.time_for_imwrite * 1000:.2f} ms')

    else:
        print('Compositor OFF.')


if __name__ == '__main__':
    # expect zero or one argument: name of the config file
    if len(sys.argv) < 2:
        main(CONFIG_FILE)
    else:
        main(sys.argv[1])
